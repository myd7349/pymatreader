# simple makefile to simplify repetetive build env management tasks under posix

# caution: testing won't work on windows, see README

VERSION_FILE:=pymatreader/_version.py
VERSION:=$(strip $(filter-out __version__ =,$(shell cat ${VERSION_FILE})))
PYPIVERSION:=$(subst _,.,$(VERSION))
pypidist:=dist/pymatreader-$(PYPIVERSION).tar.gz

ifeq ($(findstring dev,$(VERSION)), dev)
	export TWINE_REPOSITORY_URL=https://test.pypi.org/legacy/
	ifeq ($(shell echo -n $(PYPIVERSION) | tail -c 1), v)
		PYPIVERSION:=$(PYPIVERSION)0
	endif
	ISDEV:=1
else
	ISDEV:=0
endif

print-version:
	@echo $(VERSION)
	@echo ${PYPIVERSION}

flake:
	@if command -v flake8 > /dev/null; then \
		echo "Running flake8"; \
		flake8 --count pymatreader; \
	else \
		echo "flake8 not found, please install it!"; \
		exit 1; \
	fi;
	@echo "flake8 passed"

code_quality:
	docker run --interactive --tty --rm --env \
	CODECLIMATE_CODE="$(CURDIR)" --volume "$(CURDIR)":/code \
	--volume /var/run/docker.sock:/var/run/docker.sock \
	--volume /tmp/cc:/tmp/cc  codeclimate/codeclimate analyze

build-doc:
	cd doc; make clean; make html

autobuild-doc:
	sphinx-autobuild doc/source doc/build

clean-dist:
	rm -rf dist
	rm -rf pymatreader.egg-info

$(pypidist):
	python setup.py sdist bdist_wheel

make-dist: $(pypidist)

upload-dist: make-dist
	twine upload dist/pymatreader-$(PYPIVERSION).tar.gz dist/pymatreader-$(PYPIVERSION)-*.whl

tag-release:
ifeq ($(ISDEV), 0)
	git tag -a v$(VERSION) -m "version $(VERSION)"
	git push origin v$(VERSION)
endif
