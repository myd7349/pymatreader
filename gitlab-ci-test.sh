#!/bin/sh
cd $CI_PROJECT_DIR
echo $PIP_PRE
apt-get update
apt-get install -y libhdf5-dev
if [ $PIP_PRE = "1" ]; then
    echo "using pre versions"
    pip install --pre -U -r requirements.txt
    pip install --progress-bar off --upgrade --pre --only-binary ":all:" --no-deps -i "https://pypi.anaconda.org/scipy-wheels-nightly/simple" numpy scipy
else
    echo "using stable versions"
    pip install -U -r requirements.txt
fi
pip freeze
pytest --cov=pymatreader && \
codecov
